package titask.websocket.web;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BoardSocketController {

    private final SimpMessagingTemplate simpMessagingTemplate;

    public BoardSocketController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/socket/{id}")
    public void board(@PathVariable(value = "id") Integer id) {
        this.simpMessagingTemplate.convertAndSend("/board/socket/"+id, "");
    }

}
